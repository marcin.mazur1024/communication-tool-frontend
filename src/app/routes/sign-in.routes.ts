import { Routes } from '@angular/router';

export const signInRoutes: Routes = [
  {
    path: '',
    loadComponent: () => import('../pages/sign-in-page/sign-in-page.component').then((c) => c.SignInPageComponent),
  },
];
