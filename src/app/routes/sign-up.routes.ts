import { Routes } from '@angular/router';

export const signUpRoutes: Routes = [
  {
    path: '',
    loadComponent: () => import('../pages/sign-up-page/sign-up-page.component').then((c) => c.SignUpPageComponent),
  },
];
