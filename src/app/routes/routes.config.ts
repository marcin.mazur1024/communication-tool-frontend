export const ROUTES = {
  BASE: '',
  WILDCARD: '**',
  LOGOUT: 'logout',
  SIGN_IN: 'sign-in',
  SIGN_UP: 'sign-up',
  COMMUNICATOR: 'communicator',
};
