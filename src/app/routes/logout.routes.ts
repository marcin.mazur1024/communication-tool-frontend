import { Routes } from '@angular/router';

export const logoutRoutes: Routes = [
  {
    path: '',
    loadComponent: () => import('../pages/logout/logout.component').then((c) => c.LogoutComponent),
  },
];
