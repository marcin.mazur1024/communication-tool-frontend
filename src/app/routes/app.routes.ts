import { Routes } from '@angular/router';
import { ROUTES as R } from './routes.config';
import { authorizedGuard } from '@guards/authorized/authorized.guard';
import { unauthorizedGuard } from '@guards/unauthorized/unauthorized.guard';

export const routes: Routes = [
  {
    path: R.SIGN_IN,
    canActivate: [unauthorizedGuard],
    loadChildren: () => import('./sign-in.routes').then((r) => r.signInRoutes),
  },
  {
    path: R.SIGN_UP,
    canActivate: [unauthorizedGuard],
    loadChildren: () => import('./sign-up.routes').then((r) => r.signUpRoutes),
  },
  {
    path: R.LOGOUT,
    loadChildren: () => import('./logout.routes').then((r) => r.logoutRoutes),
  },
  {
    path: R.COMMUNICATOR,
    canActivate: [authorizedGuard],
    loadChildren: () => import('./communicator.routes').then((r) => r.communicatorRoutes),
  },
  {
    path: R.WILDCARD,
    redirectTo: '/sign-in',
  },
];
