import { Routes } from '@angular/router';
import { ROUTES } from './routes.config';

export const communicatorRoutes: Routes = [
  {
    path: ROUTES.BASE,
    loadComponent: () => import('../pages/communicator/communicator.component').then((c) => c.CommunicatorComponent),
  },
];
