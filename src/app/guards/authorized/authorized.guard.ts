import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { StorageKey } from '@enums/storage-key.enum';
import { environment } from '@env';
import { LocalStorageService } from '@services/local-storage.service';

export const authorizedGuard: CanActivateFn = (route, state) => {
  const localStorageService = inject(LocalStorageService);
  const router = inject(Router);
  const token = localStorageService.get(StorageKey.TOKEN);
  if (!token) {
    router.navigate([environment.baseUnAuthRoute]);
    return false;
  } else return true;
};
