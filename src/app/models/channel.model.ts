export class Channel {
  userId!: number;
  channelId!: number;
  username!: string;
  content!: string;
  createdAt!: Date;

  constructor(data: Channel) {
    Object.assign(this, data);
    this.createdAt = new Date(data.createdAt);
  }
}
