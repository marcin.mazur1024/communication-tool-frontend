export class Message {
  id!: number;
  channelId!: number;
  username!: string;
  userId!: number;
  content!: string;
  createdAt!: Date;

  constructor(data: Message) {
    Object.assign(this, data);
    this.createdAt = new Date(data.createdAt);
  }
}
