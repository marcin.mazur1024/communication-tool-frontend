export enum StorageKey {
  TOKEN = 'TOKEN',
  USER_CONTEXT = 'USER_CONTEXT',
}
