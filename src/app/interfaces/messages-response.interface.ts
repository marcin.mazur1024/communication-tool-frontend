import { Message } from "@models/message.model";

export interface MessagesResponse {
  messages: Message[];
}
