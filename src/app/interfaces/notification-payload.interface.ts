export interface NotificationPayload {
  message: string;
  title?: string;
  skipTranslate?: boolean;
  severity: 'success' | 'info' | 'warn' | 'error',
}
