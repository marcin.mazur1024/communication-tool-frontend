export interface Environment {
  apiUrl: string;
  production: boolean;
  baseAuthRoute: string;
  baseUnAuthRoute: string;
  wssAuthUrl: string;
  wssBroadcaster: string,
  wssHost: string,
  wssPort: number,
  wssKey: string,
  wssForceTls: boolean,
  wssDisableStats: boolean,
}
