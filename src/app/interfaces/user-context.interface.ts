import { UserRole } from "@enums/user-role.enum";

export interface UserContext {
  id: number;
  username: string;
  role: UserRole;
}
