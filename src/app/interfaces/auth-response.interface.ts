import { UserContext } from "./user-context.interface";

export interface AuthResponse {
  user: UserContext;
  token: string;
}
