import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { InitializationComponent } from '@components/initialization/initialization.component';
import { NotificationsComponent } from '@components/notifications/notifications.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    NotificationsComponent,
    InitializationComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  providers: [],
})
export class AppComponent { }
