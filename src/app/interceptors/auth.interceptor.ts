import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { StorageKey } from '@enums/storage-key.enum';
import { LocalStorageService } from '@services/local-storage.service';

export const authInterceptor: HttpInterceptorFn = (req, next) => {

  const storage = inject(LocalStorageService);

  const authToken = storage.get(StorageKey.TOKEN);

  const headers: any = {};

  if (authToken) headers['Authorization'] = `Bearer ${authToken}`;

  const authReq = req.clone({
    setHeaders: headers
  });

  return next(authReq);
};
