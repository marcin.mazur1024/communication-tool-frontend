import { HttpErrorResponse, HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '@env';
import { catchError, throwError } from 'rxjs';

export const errorInterceptor: HttpInterceptorFn = (req, next) => {

  const router = inject(Router);

  return next(req).pipe(
    catchError((err: any) => {
      if (err instanceof HttpErrorResponse) {
        switch (err.status) {
          case 401:
          case 403:
            router.navigate([`/${environment.baseUnAuthRoute}`])
            break;
          default:
            break;
        }
      } else
        console.error('An error occurred:', err);

      return throwError(() => err);
    })
  );
};
