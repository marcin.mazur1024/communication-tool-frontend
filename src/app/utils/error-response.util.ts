import { Observable, of } from "rxjs";

export function errorResponse<T = any>(): Observable<T> {
  return of({} as T);
}
