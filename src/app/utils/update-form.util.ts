import { AbstractControl, FormArray, FormControl, FormGroup } from '@angular/forms';

export function updateForm(control: AbstractControl): void {
  if (control instanceof FormControl) {
    control.markAsDirty();
    control.markAsTouched();
  } else if (control instanceof FormGroup || control instanceof FormArray) {
    for (const innerControl of Object.values(control.controls)) {
      updateForm(innerControl);
    }
  }
}
