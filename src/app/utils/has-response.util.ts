export function hasResponse(response: any): boolean {
  return !!response && !!Object.entries(response).length;
}
