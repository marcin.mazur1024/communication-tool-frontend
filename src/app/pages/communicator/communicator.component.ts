import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MessageLogComponent } from '@components/message-log/message-log.component';
import { TitleComponent } from '@components/title/title.component';
import { TopBarComponent } from '@components/top-bar/top-bar.component';

@Component({
  selector: 'ct-communicator',
  standalone: true,
  imports: [CommonModule, TopBarComponent, MessageLogComponent],
  template: `<ct-top-bar/><ct-message-log />`,
  styleUrl: './communicator.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommunicatorComponent extends TitleComponent implements OnInit {
  readonly pageTitle: string = 'Title.Communicator';

  ngOnInit(): void {
    this.setTitle(this.pageTitle);
  }
}
