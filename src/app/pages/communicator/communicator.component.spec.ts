import { ComponentFixture, TestBed } from "@angular/core/testing";
import { CommunicatorComponent } from "./communicator.component"
import { TranslateModule } from "@ngx-translate/core";
import { CUSTOM_ELEMENTS_SCHEMA, Component } from "@angular/core";
import { TopBarComponent } from "@components/top-bar/top-bar.component";
import { MessageLogComponent } from "@components/message-log/message-log.component";

@Component({
  selector: 'ct-top-bar',
  standalone: true,
  template: ``,
})
class TopBarComponentMock { }

@Component({
  selector: 'ct-message-log',
  standalone: true,
  template: '',
})
export class MessageLogComponentMock { }

describe('CommunicatorComponent', () => {

  let component: CommunicatorComponent;
  let fixture: ComponentFixture<CommunicatorComponent>;

  beforeEach(async () => {

    await TestBed
      .overrideComponent(CommunicatorComponent, {
        remove: { imports: [TopBarComponent, MessageLogComponent] },
        add: { imports: [TopBarComponentMock, MessageLogComponentMock] }
      })
      .configureTestingModule({
        imports: [
          CommunicatorComponent,
          TranslateModule.forRoot({}),
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();

    fixture = TestBed.createComponent(CommunicatorComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the title', async () => {

    const spy = spyOn(component, 'setTitle');
    fixture.detectChanges();
    await fixture.whenStable();

    expect(spy).toHaveBeenCalledWith(component.pageTitle);

  });

})
