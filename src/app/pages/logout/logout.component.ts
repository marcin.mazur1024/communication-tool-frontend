import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import { TitleComponent } from '@components/title/title.component';
import { Store } from '@ngrx/store';
import { ROUTES } from '@routes/routes.config';
import { AuthHttpService } from '@services/auth-http.service';
import { LocalStorageService } from '@services/local-storage.service';
import { WebsocketService } from '@services/websocket.service';
import { Subject, switchMap } from 'rxjs';
import { updateUserContext } from 'src/app/states/user/user.action';

@Component({
  selector: 'ct-logout',
  standalone: true,
  imports: [CommonModule],
  template: ``,
  styleUrl: './logout.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoutComponent extends TitleComponent implements OnInit, OnDestroy {
  private readonly router = inject(Router);
  private readonly localStorageService = inject(LocalStorageService);
  private readonly authHttpService = inject(AuthHttpService);
  private readonly websocketService = inject(WebsocketService);
  private readonly store = inject(Store);
  readonly pageTitle: string = 'Title.Logout';

  private readonly logoutRequest$ = new Subject<boolean>();

  ngOnInit(): void {
    this.handleLogoutRequest();
    this.setTitle(this.pageTitle);
    this.logoutRequest$.next(true);
  }

  ngOnDestroy(): void {
    this.logoutRequest$.complete();
  }

  private clearStorage(): void {
    this.localStorageService.clear();
  };

  private navigateToHome(): void {
    this.router.navigate([`/${ROUTES.SIGN_IN}`]);
  };

  private finilizeLogout(): void {
    this.store.dispatch(updateUserContext({ userContext: undefined }));
    this.websocketService.disconnect();
    this.clearStorage();
    this.navigateToHome();
  }

  private handleLogoutRequest(): void {
    this.logoutRequest$.pipe(
      switchMap(() => this.authHttpService.logout())
    ).subscribe({
      next: () => this.finilizeLogout(),
      error: () => this.finilizeLogout(),
    });
  }
}
