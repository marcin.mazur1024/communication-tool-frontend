import { ComponentFixture, TestBed } from "@angular/core/testing";
import { LogoutComponent } from "./logout.component";
import { TranslateModule } from "@ngx-translate/core";
import { Store, provideStore } from "@ngrx/store";
import { LocalStorageService } from "@services/local-storage.service";
import { AuthHttpService } from "@services/auth-http.service";
import { WebsocketService } from "@services/websocket.service";
import { MockStore } from "@ngrx/store/testing";
import { updateUserContext } from "src/app/states/user/user.action";
import { Router } from "@angular/router";

describe('LogoutComponent', () => {

  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  let localStorageService: jasmine.SpyObj<LocalStorageService>;
  let authHttpService: jasmine.SpyObj<AuthHttpService>;
  let router: jasmine.SpyObj<Router>;
  let websocketService: jasmine.SpyObj<WebsocketService>;
  let store: MockStore;

  beforeEach(async () => {

    await TestBed
      .configureTestingModule({
        imports: [
          LogoutComponent,
          TranslateModule.forRoot({}),
        ],
        providers: [
          provideStore(),
          { provide: LocalStorageService, useValue: jasmine.createSpyObj('LocalStorageService', ['clear']) },
          { provide: AuthHttpService, useValue: jasmine.createSpyObj('AuthHttpService', ['logout']) },
          { provide: WebsocketService, useValue: jasmine.createSpyObj('WebsocketService', ['disconnect']) },
          { provide: Router, useValue: jasmine.createSpyObj('Router', ['navigate']) },
        ]
      }).compileComponents();

    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
    localStorageService = TestBed.inject(LocalStorageService) as jasmine.SpyObj<LocalStorageService>;
    authHttpService = TestBed.inject(AuthHttpService) as jasmine.SpyObj<AuthHttpService>;
    websocketService = TestBed.inject(WebsocketService) as jasmine.SpyObj<WebsocketService>;
    router = TestBed.inject(Router) as jasmine.SpyObj<Router>;
    store = TestBed.inject(Store) as MockStore;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should clear localStorage', async () => {
    spyOn(store, 'dispatch');
    component.ngOnInit();
    fixture.detectChanges();
    await fixture.whenStable();

    expect(store.dispatch).toHaveBeenCalledWith(updateUserContext({ userContext: undefined }));
    expect(localStorageService.clear).toHaveBeenCalled();
    expect(authHttpService.logout).toHaveBeenCalled();
    expect(websocketService.disconnect).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalled();
  });

})
