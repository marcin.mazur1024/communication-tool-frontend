import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthFormWrapperComponent } from '@components/auth-form-wrapper/auth-form-wrapper.component';
import { AuthFormComponent } from '@components/auth-form/auth-form.component';
import { TitleComponent } from '@components/title/title.component';
import { AuthFormType } from '@enums/auth-form-type.enum';
import { ROUTES } from '@routes/routes.config';

@Component({
  selector: 'ct-sign-up-page',
  standalone: true,
  imports: [CommonModule, AuthFormWrapperComponent, AuthFormComponent],
  template: `
  <ct-auth-form-wrapper [footerUrl]="url" [footerLabel]="'AuthForm.LogInUsingExisting'" [headerlabel]="'AuthForm.SignUp'">
    <ct-auth-form authForm [formType]="AuthFormType.SING_UP"></ct-auth-form>
  </ct-auth-form-wrapper>
  `,
  styleUrl: './sign-up-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignUpPageComponent extends TitleComponent implements OnInit {

  readonly pageTitle: string = 'Title.SignUp';
  readonly url = ROUTES.SIGN_IN;
  readonly AuthFormType = AuthFormType;

  ngOnInit(): void {
    this.setTitle(this.pageTitle);
  }
}
