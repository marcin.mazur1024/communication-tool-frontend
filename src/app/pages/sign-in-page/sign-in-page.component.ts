import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthFormWrapperComponent } from '@components/auth-form-wrapper/auth-form-wrapper.component';
import { AuthFormComponent } from '@components/auth-form/auth-form.component';

import { TitleComponent } from '@components/title/title.component';
import { AuthFormType } from '@enums/auth-form-type.enum';
import { ROUTES } from '@routes/routes.config';

@Component({
  selector: 'ct-sign-in-page',
  standalone: true,
  imports: [CommonModule, AuthFormWrapperComponent, AuthFormComponent],
  template: `
  <ct-auth-form-wrapper [footerUrl]="ROUTES.SIGN_UP" [footerLabel]="'AuthForm.CreateNewAccount'" [headerlabel]="'AuthForm.SignIn'">
    <ct-auth-form authForm [formType]="AuthFormType.SING_IN"></ct-auth-form>
  </ct-auth-form-wrapper>`,
  styleUrl: './sign-in-page.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignInPageComponent extends TitleComponent implements OnInit {

  private readonly pageTitle: string = 'Title.SignIn';
  readonly AuthFormType = AuthFormType;
  readonly ROUTES = ROUTES;

  ngOnInit(): void {
    this.setTitle(this.pageTitle);
  }
}
