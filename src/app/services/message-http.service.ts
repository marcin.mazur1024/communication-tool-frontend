import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { MessagesResponse } from '@interfaces/messages-response.interface';
import { HttpParams } from '@angular/common/http';
import { MessagePayload } from '@components/message-log/message-log.service';

@Injectable({
  providedIn: 'root'
})
export class MessageHttpService extends HttpService {

  fetchMessages(channelId: number): Observable<MessagesResponse> {
    const params = new HttpParams().set('channelId', channelId);
    return this.http.get<MessagesResponse>(this.apiUrl + 'message', { params });
  }

  sendMessage(payload: MessagePayload): Observable<MessagesResponse> {
    return this.http.post<MessagesResponse>(this.apiUrl + 'message', { ...payload });
  }
}
