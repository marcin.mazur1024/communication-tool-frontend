import { Injectable } from '@angular/core';
import { NotificationPayload } from '@interfaces/notification-payload.interface';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private readonly _notify$ = new Subject<NotificationPayload>();
  readonly notify$ = this._notify$.asObservable();

  success = (message: string, title: string | undefined = undefined, skipTranslate: boolean = false): void => {
    this._notify$.next({ message, severity: 'success', skipTranslate, title });
  };

  info = (message: string, title: string | undefined = undefined, skipTranslate: boolean = false): void => {
    this._notify$.next({ message, severity: 'info', title, skipTranslate });
  };

  warn = (message: string, title: string | undefined = undefined, skipTranslate: boolean = false): void => {
    this._notify$.next({ message, severity: 'warn', skipTranslate, title });
  };

  error = (message: string, title: string | undefined = undefined, skipTranslate: boolean = false): void => {
    this._notify$.next({ message, severity: 'error', skipTranslate, title });
  };

}
