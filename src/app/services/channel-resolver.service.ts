import { Injectable, inject } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { LocalStorageService } from "./local-storage.service";
import { UserContext } from "@interfaces/user-context.interface";
import { StorageKey } from "@enums/storage-key.enum";
import { UserRole } from "@enums/user-role.enum";

@Injectable()
export class ChannelResolverService {

  private readonly localStorageService = inject(LocalStorageService);
  userContext: UserContext | undefined = undefined;

  private readonly _channelId$ = new BehaviorSubject<number | undefined>(undefined);
  readonly channelId$ = this._channelId$.asObservable();

  get channelId(): number | undefined {
    return this._channelId$.getValue();
  }

  set channelId(value: number | undefined) {
    this._channelId$.next(value);
  }

  get canSwitchChannels(): boolean {
    return UserRole.COMMAND_DEVICE === this.userContext?.role;
  }

  constructor() {
    const user = this.localStorageService.get(StorageKey.USER_CONTEXT);
    this.userContext = user ? JSON.parse(user) : undefined;
  }

  initialize(): void {
    if (!this.userContext) return;
    const initialChannelId = UserRole.FIELD_DEVICE === this.userContext.role ? this.userContext.id : undefined;
    this._channelId$.next(initialChannelId);
  }

}
