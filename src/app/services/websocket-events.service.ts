import { Injectable } from '@angular/core';
import { Message } from '@models/message.model';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WebsocketEventsService {

  readonly newMessage$ = new Subject<Message>();

}
