import { Injectable, inject } from '@angular/core';
import { HttpError } from '@enums/http-error.enum';
import { NotificationService } from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class HttpErrorHandlerService {

  private readonly notificationService = inject(NotificationService);

  errorMessage(error: HttpError): void {
    this.notificationService.error(this.resolveErrorMessage(error));
  }

  private resolveErrorMessage(error: HttpError): string {
    switch (error) {
      case HttpError.INVALID_CREDENTIALS:
        return 'Error.InvalidCredentials';
      case HttpError.USERNAME_NOT_UNIQUE:
        return 'Error.UsernameNotUnique';
      case HttpError.INVALID_INPUT_DATA:
        return 'Error.InvalidInputData';
      case HttpError.FORBIDDEN:
        return 'Error.Forbidden';
      case HttpError.UNAUTHORIZED:
        return 'Error.Unauthorized';
      default:
        return 'Error.GeneralHttp';
    }
  }

}
