import { Injectable, inject } from '@angular/core';
import Echo from 'laravel-echo';
import { LocalStorageService } from './local-storage.service';
import { UserContext } from '@interfaces/user-context.interface';
import { StorageKey } from '@enums/storage-key.enum';
import { environment } from '@env';
import { WebsocketEvent } from '@enums/websocket-event.enum';
import { WebsocketEventsService } from './websocket-events.service';
import { Message } from '@models/message.model';

const CHANNEL: string = 'channel';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  private readonly localStorage = inject(LocalStorageService);
  private readonly eventsService = inject(WebsocketEventsService);

  private echo: Echo | undefined = undefined;

  constructor() { }

  connect(): void {
    if (this.echo) return;
    const userContext = this.getUserContext();
    if (!userContext) return;
    this.echo = new Echo({
      broadcaster: environment.wssBroadcaster,
      authEndpoint: environment.wssAuthUrl,
      key: environment.wssKey,
      wsHost: environment.wssHost,
      wsPort: environment.wssPort,
      wssPort: environment.wssPort,
      forceTLS: environment.wssForceTls,
      enabledTransports: ['ws', 'wss'],
      auth: {
        headers: {
          Authorization: 'Bearer ' + this.localStorage.get(StorageKey.TOKEN)
        }
      },
    });
  }

  disconnect(): void {
    if (!this.echo) return;
    this.echo.disconnect();
    this.echo = undefined;
  }

  joinPrivateChannel(channelId: number): void {
    if (!this.echo) return;
    this.echo.private(`${CHANNEL}.${channelId}`).listen(WebsocketEvent.NEW_MESSAGE, (event: any) => {
      this.eventsService.newMessage$.next(new Message(event.message));
    });
  }

  leavePrivateChannels(): void {
    if (!this.echo) return;
    this.echo.leaveAllChannels();
  }

  private getUserContext(): UserContext | undefined {
    const user = this.localStorage.get(StorageKey.USER_CONTEXT);
    return user ? JSON.parse(user) : undefined;
  }

}
