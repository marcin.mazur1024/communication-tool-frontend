import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { Observable } from "rxjs";
import { AuthPayload } from "@interfaces/auth-payload.interface";
import { AuthResponse } from "@interfaces/auth-response.interface";

@Injectable({
  providedIn: 'root',
})
export class AuthHttpService extends HttpService {

  createNewAccount(payload: AuthPayload): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(this.apiUrl + 'user', { ...payload });
  }

  login(payload: AuthPayload): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(this.apiUrl + 'user/login', { ...payload });
  }

  logout(): Observable<boolean> {
    return this.http.post<boolean>(this.apiUrl + 'user/logout', {});
  }
}
