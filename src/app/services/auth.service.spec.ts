import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { LocalStorageService } from './local-storage.service';
import { StorageKey } from '@enums/storage-key.enum';
import { environment } from '@env';
import { AuthResponse } from '@interfaces/auth-response.interface';
import { UserRole } from '@enums/user-role.enum';
import { Store } from '@ngrx/store';
import { WebsocketService } from './websocket.service';
import { updateUserContext } from '../states/user/user.action';

describe('AuthService', () => {
  let authService: AuthService;
  let routerSpyObj: jasmine.SpyObj<Router>;
  let storeSpyObj: jasmine.SpyObj<Store>;
  let localStorageServiceSpyObj: jasmine.SpyObj<LocalStorageService>;
  let websocketServiceSpyObj: jasmine.SpyObj<WebsocketService>;

  const authResponse: AuthResponse = {
    user: {
      id: 1,
      role: UserRole.FIELD_DEVICE,
      username: 'FIELD_DEVICE_1',
    },
    token: 'authToken'
  };

  beforeEach(() => {
    routerSpyObj = jasmine.createSpyObj('Router', ['navigate']);
    storeSpyObj = jasmine.createSpyObj('Store', ['dispatch']);
    localStorageServiceSpyObj = jasmine.createSpyObj('LocalStorageService', ['set', 'get']);
    websocketServiceSpyObj = jasmine.createSpyObj('WebsocketService', ['connect']);

    TestBed.configureTestingModule({
      providers: [
        AuthService,
        { provide: Router, useValue: routerSpyObj },
        { provide: LocalStorageService, useValue: localStorageServiceSpyObj },
        { provide: Store, useValue: storeSpyObj },
        { provide: WebsocketService, useValue: websocketServiceSpyObj },
      ],
    });

    authService = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should update local store user and store user, connect websocket and navigate to auth', () => {
    authService.login(authResponse);
    expect(localStorageServiceSpyObj.set).toHaveBeenCalledWith(StorageKey.USER_CONTEXT, authResponse.user);
    expect(localStorageServiceSpyObj.set).toHaveBeenCalledWith(StorageKey.TOKEN, authResponse.token);
    expect(storeSpyObj.dispatch).toHaveBeenCalledWith(updateUserContext({ userContext: authResponse.user }));
    expect(websocketServiceSpyObj.connect).toHaveBeenCalled();
    expect(routerSpyObj.navigate).toHaveBeenCalledWith([`/${environment.baseAuthRoute}`]);
  });

});
