import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthHttpService } from './auth-http.service';
import { AuthPayload } from '@interfaces/auth-payload.interface';
import { AuthResponse } from '@interfaces/auth-response.interface';
import { UserRole } from '@enums/user-role.enum';

describe('AuthHttpService', () => {
  let service: AuthHttpService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthHttpService]
    });
    service = TestBed.inject(AuthHttpService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('createNewAccount', () => {
    it('should create new account', (done: DoneFn) => {
      const mockPayload: AuthPayload = { password: 'pass', username: 'user' };
      const mockResponse: AuthResponse = { token: 'authToken', user: { id: 100, role: UserRole.FIELD_DEVICE, username: 'user' } };

      service.createNewAccount(mockPayload).subscribe(response => {
        expect(response).toEqual(mockResponse);
        done();
      });

      const req = httpTestingController.expectOne(request => request.url.includes('/user'));
      expect(req.request.method).toEqual('POST');
      req.flush(mockResponse);
    });
  });

  describe('login', () => {
    it('should login', (done: DoneFn) => {
      const mockPayload: AuthPayload = { password: 'pass', username: 'user' };
      const mockResponse: AuthResponse = { token: 'authToken', user: { id: 100, role: UserRole.FIELD_DEVICE, username: 'user' } };

      service.login(mockPayload).subscribe(response => {
        expect(response).toEqual(mockResponse);
        done();
      });

      const req = httpTestingController.expectOne(request => request.url.includes('/user/login'));
      expect(req.request.method).toEqual('POST');
      req.flush(mockResponse);
    });
  });

  describe('logout', () => {
    it('should logout', (done: DoneFn) => {
      const mockResponse: boolean = true;

      service.logout().subscribe(response => {
        expect(response).toEqual(mockResponse);
        done();
      });

      const req = httpTestingController.expectOne(request => request.url.includes('/user/logout'));
      expect(req.request.method).toEqual('POST');
      req.flush(mockResponse);
    });
  });

});
