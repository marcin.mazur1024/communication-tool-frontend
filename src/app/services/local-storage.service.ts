import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { StorageKey } from '@enums/storage-key.enum';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor(@Inject(DOCUMENT) private document: Document) { }
  readonly localStorage = this.document.defaultView?.localStorage;

  get = (key: StorageKey): string | null => {
    if (!this.localStorage) return null;
    return this.localStorage.getItem(key.toString());
  };

  set = (key: StorageKey, value: any): void => {
    if (!this.localStorage) return;
    if (value === null || value === undefined) return;
    return this.localStorage.setItem(key.toString(), typeof value === 'string' ? value : JSON.stringify(value));
  };

  remove = (key: StorageKey): void => {
    if (!this.localStorage) return;
    return this.localStorage.removeItem(key.toString());
  };

  clear = (): void => {
    if (!this.localStorage) return;
    return this.localStorage.clear();
  };
}
