import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';
import { Channel } from '@models/channel.model';

export interface ChannelResponse {
  channels: Channel[];
}

@Injectable({
  providedIn: 'root'
})
export class ChannelHttpService extends HttpService {

  fetchChannels(): Observable<ChannelResponse> {
    return this.http.get<ChannelResponse>(this.apiUrl + 'channel');
  }

}
