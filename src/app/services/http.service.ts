import { HttpClient } from "@angular/common/http";
import { inject } from "@angular/core";
import { environment } from "@env";

export abstract class HttpService {
  readonly http = inject(HttpClient)
  readonly apiUrl = environment.apiUrl;
}
