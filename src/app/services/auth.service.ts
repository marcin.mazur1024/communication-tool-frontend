import { Injectable, inject } from "@angular/core";
import { UserContext } from "@interfaces/user-context.interface";
import { Router } from "@angular/router";
import { environment } from "@env";
import { LocalStorageService } from "./local-storage.service";
import { StorageKey } from "@enums/storage-key.enum";
import { AuthResponse } from "@interfaces/auth-response.interface";
import { WebsocketService } from "./websocket.service";
import { Store } from "@ngrx/store";
import { updateUserContext } from "../states/user/user.action";

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  private readonly router = inject(Router);
  private readonly storageService = inject(LocalStorageService);
  private readonly websocketService = inject(WebsocketService);
  private readonly store = inject(Store);

  login(authResponse: AuthResponse): void {
    this.updateLocalStoreUser(authResponse.user, authResponse.token);
    this.updateStoreUser(authResponse.user);
    this.websocketService.connect();
    this.navigateToAuth();
  }

  private updateStoreUser(user: UserContext): void {
    this.store.dispatch(updateUserContext({ userContext: user }));
  }

  private updateLocalStoreUser(userContext: UserContext, token: string): void {
    this.storageService.set(StorageKey.USER_CONTEXT, userContext);
    this.storageService.set(StorageKey.TOKEN, token);
  }

  private navigateToAuth(): void {
    this.router.navigate([`/${environment.baseAuthRoute}`]);
  }

}
