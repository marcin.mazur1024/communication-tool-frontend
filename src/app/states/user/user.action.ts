import { UserContext } from "@interfaces/user-context.interface";
import { createAction, props } from "@ngrx/store";

export const updateUserContext = createAction(
  '[USER] Update user context',
  props<{ userContext: UserContext | undefined }>()
);
