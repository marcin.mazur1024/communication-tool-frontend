import { createSelector } from "@ngrx/store";
import { UserState } from "./user.reducer";

export const selectUserState = (state: any) => state.user;

export const selectUserContext = createSelector(
  selectUserState,
  (state: UserState) => state.userContext
);
