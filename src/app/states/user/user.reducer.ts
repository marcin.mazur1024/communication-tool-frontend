import { createReducer, on } from '@ngrx/store';
import { UserContext } from '@interfaces/user-context.interface';
import { updateUserContext } from './user.action';

export interface UserState {
  userContext: UserContext | undefined;
}

export const initialUserState: UserState = {
  userContext: undefined,
};

export const userReducer = createReducer(
  initialUserState,
  on(updateUserContext, (state, { userContext }) => {
    return {
      ...state,
      userContext: userContext,
    };
  }),
);
