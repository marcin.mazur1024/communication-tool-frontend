import { TestBed, ComponentFixture } from '@angular/core/testing';
import { InitializationComponent } from './initialization.component';
import { LocalStorageService } from '@services/local-storage.service';
import { WebsocketService } from '@services/websocket.service';
import { Store } from '@ngrx/store';
import { StorageKey } from '@enums/storage-key.enum';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { updateUserContext } from 'src/app/states/user/user.action';
import { UserContext } from '@interfaces/user-context.interface';
import { UserRole } from '@enums/user-role.enum';

describe('InitializationComponent', () => {
  let component: InitializationComponent;
  let fixture: ComponentFixture<InitializationComponent>;
  let localStorageServiceMock: jasmine.SpyObj<LocalStorageService>;
  let websocketServiceMock: jasmine.SpyObj<WebsocketService>;
  let store: MockStore;
  const userContext: UserContext = { username: 'test', id: 100, role: UserRole.FIELD_DEVICE };
  const token: string = 'authToken';

  beforeEach(async () => {
    localStorageServiceMock = jasmine.createSpyObj('LocalStorageService', ['get']);
    websocketServiceMock = jasmine.createSpyObj('WebsocketService', ['connect']);

    await TestBed.configureTestingModule({
      imports: [InitializationComponent],
      providers: [
        { provide: LocalStorageService, useValue: localStorageServiceMock },
        { provide: WebsocketService, useValue: websocketServiceMock },
        provideMockStore({})
      ]
    }).compileComponents();

    store = TestBed.inject(Store) as MockStore;
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InitializationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should connect to websocket if token exists', () => {
    localStorageServiceMock.get.withArgs(jasmine.any(String)).and.callFake((key: string) => {
      if (key === StorageKey.TOKEN) {
        return 'test-token';
      } else if (key === StorageKey.USER_CONTEXT) {
        return JSON.stringify(userContext);
      }
      return token;
    });
    component.ngOnInit();
    expect(websocketServiceMock.connect).toHaveBeenCalled();
  });

  it('should dispatch updateUserContext action with user if user context exists', () => {
    localStorageServiceMock.get.withArgs(jasmine.any(String)).and.callFake((key: string) => {
      return JSON.stringify(userContext);
    });
    spyOn(store, 'dispatch');
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(updateUserContext({ userContext }));
  });

  it('should dispatch updateUserContext action with undefined if user context does not exist', () => {
    localStorageServiceMock.get.withArgs(jasmine.any(String)).and.callFake((key: string) => {
      return null;
    });
    spyOn(store, 'dispatch');
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(updateUserContext({ userContext: undefined }));
  });
});
