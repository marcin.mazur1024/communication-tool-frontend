import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnInit, inject } from '@angular/core';
import { StorageKey } from '@enums/storage-key.enum';
import { Store } from '@ngrx/store';
import { LocalStorageService } from '@services/local-storage.service';
import { WebsocketService } from '@services/websocket.service';
import { updateUserContext } from 'src/app/states/user/user.action';

@Component({
  selector: 'ct-initialization',
  standalone: true,
  imports: [
    CommonModule,
  ],
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InitializationComponent implements OnInit {

  private readonly websocetService = inject(WebsocketService);
  private readonly localStorageService = inject(LocalStorageService);
  private readonly store = inject(Store);

  ngOnInit(): void {
    const token = this.localStorageService.get(StorageKey.TOKEN);
    if (token) this.websocetService.connect();
    const user = this.localStorageService.get(StorageKey.USER_CONTEXT);
    this.store.dispatch(updateUserContext({ userContext: user ? JSON.parse(user) : undefined }));
  }
}
