import { TestBed, ComponentFixture } from '@angular/core/testing';
import { ChannelComponent } from './channel.component';
import { Channel } from '@models/channel.model';
import { By } from '@angular/platform-browser';

describe('ChannelComponent', () => {
  let component: ChannelComponent;
  let fixture: ComponentFixture<ChannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ChannelComponent]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelComponent);
    component = fixture.componentInstance;
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should display channel information', () => {

    const channel: Channel = {
      channelId: 100,
      username: 'username',
      createdAt: new Date(),
      userId: 100,
      content: 'content'
    };

    component.channel = channel;
    fixture.detectChanges();

    const container = fixture.debugElement.query(By.css('.channel__container'));
    const usernameElement = fixture.debugElement.query(By.css('.channel__username'));
    const dateElement = fixture.debugElement.query(By.css('.channel__date'));

    expect(container).toBeTruthy();
    expect(usernameElement.nativeElement.textContent).toContain(channel.username);
    expect(dateElement.nativeElement.textContent).toContain(channel.createdAt.toUTCString());
  });

  it('should emit channelSelected event when channel is clicked', () => {
    const channel: Channel = {
      channelId: 100,
      username: 'username',
      createdAt: new Date(),
      userId: 100,
      content: 'content'
    };

    component.channel = channel;
    fixture.detectChanges();

    spyOn(component.channelSelected, 'emit');

    const container = fixture.debugElement.query(By.css('.channel__container'));
    container.triggerEventHandler('click', null);

    expect(component.channelSelected.emit).toHaveBeenCalledWith(channel.channelId);
  });
});
