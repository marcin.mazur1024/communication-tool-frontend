import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Channel } from '@models/channel.model';

@Component({
  selector: 'ct-channel',
  standalone: true,
  imports: [
    CommonModule,
  ],
  template: `
  <div class="channel__container" (click)="channelSelected.emit(channel.channelId)">
    <div class="channel__username">{{channel.username}}</div>
    <div class="channel__date">{{channel.createdAt.toUTCString()}}</div>
  </div>
  `,
  styleUrl: './channel.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelComponent {
  @Input({ required: true }) channel!: Channel;
  @Output() channelSelected = new EventEmitter<number>();
}
