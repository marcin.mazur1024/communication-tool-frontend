import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';

@Component({
  standalone: true,
  imports: [CommonModule],
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export abstract class TitleComponent {
  private readonly titlePrefix = 'Communication Tool';

  private readonly title = inject(Title);
  private readonly translateService = inject(TranslateService);

  setTitle = (title: string): void => {
    this.title.setTitle(`${this.titlePrefix} | ${this.translateService.instant(title)}`);
  };
}
