import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'primeng/button';

@Component({
  selector: 'ct-submit-button',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
    ButtonModule
  ],
  template: `
    <p-button
      label="Primary"
      type="submit"
      [loading]="isLoading"
      [label]="label | translate"
    ></p-button>
  `,
  styleUrl: './submit-button.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubmitButtonComponent {
  @Input({ required: true }) label!: string;
  @Input() isLoading: boolean = false;
}
