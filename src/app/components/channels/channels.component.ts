import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { ChannelsService } from './channels.service';
import { BehaviorSubject, combineLatest, map } from 'rxjs';
import { SidebarModule } from 'primeng/sidebar';
import { ButtonModule } from 'primeng/button';
import { TranslateModule } from '@ngx-translate/core';
import { ChannelComponent } from '@components/channel/channel.component';
import { SpinnerComponent } from '@components/spinner/spinner.component';
import { ChannelResolverService } from '@services/channel-resolver.service';

@Component({
  selector: 'ct-channels',
  standalone: true,
  imports: [
    CommonModule,
    SidebarModule,
    ButtonModule,
    TranslateModule,
    ChannelComponent,
    SpinnerComponent,
  ],
  styleUrl: './channels.component.scss',
  templateUrl: './channels.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ChannelsService]
})
export class ChannelsComponent {

  private readonly channelsService = inject(ChannelsService);
  private readonly channelResolver = inject(ChannelResolverService);

  private readonly _sidebarVisible$ = new BehaviorSubject<boolean>(false);

  vm$ = combineLatest([
    this._sidebarVisible$.asObservable(),
    this.channelsService.channels$,
    this.channelsService.isLoading$,
  ]).pipe(map(([sidebarVisible, channels, isLoading]) => ({ sidebarVisible, channels, isLoading })));

  onToggleSidebar(sidebarVisible: boolean): void {
    if (sidebarVisible) this.channelsService.fetchChannels$.next(true);
    this._sidebarVisible$.next(sidebarVisible);
  }

  onChannelSelected(channelId: number): void {
    this.channelResolver.channelId = channelId;
    this.onToggleSidebar(false);
  }

}
