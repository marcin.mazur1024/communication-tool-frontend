import { Injectable, OnDestroy, inject } from '@angular/core';
import { Destroyable } from '@utils/destroyable.util';
import { Channel } from '@models/channel.model';
import { ChannelHttpService, ChannelResponse } from '@services/channel-http.service';
import { HttpErrorHandlerService } from '@services/http-error-handler.service';
import { errorResponse } from '@utils/error-response.util';
import { hasResponse } from '@utils/has-response.util';
import { BehaviorSubject, Subject, catchError, switchMap, takeUntil, tap } from 'rxjs';

@Injectable()
export class ChannelsService extends Destroyable implements OnDestroy {

  private readonly httpService = inject(ChannelHttpService);
  private readonly httpErrorHandler = inject(HttpErrorHandlerService);

  readonly channels$ = new BehaviorSubject<Channel[]>([]);
  readonly isLoading$ = new BehaviorSubject<boolean>(false);

  readonly fetchChannels$ = new Subject<boolean>();

  constructor() {
    super();
    this.handleFetchChannels();
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.fetchChannels$.complete();
  }

  private handleFetchChannels(): void {
    this.fetchChannels$.pipe(
      tap(() => this.isLoading$.next(true)),
      switchMap(() => this.httpService.fetchChannels().pipe(
        takeUntil(this.destroy$),
        catchError((error) => {
          this.httpErrorHandler.errorMessage(error?.error?.error);
          return errorResponse<ChannelResponse>();
        })
      )),
    ).subscribe({
      next: (response) => {
        if (hasResponse(response))
          this.channels$.next(response.channels.map(c => new Channel(c)));
        this.isLoading$.next(false);
      },
      error: () => this.isLoading$.next(false),
    });
  }

}
