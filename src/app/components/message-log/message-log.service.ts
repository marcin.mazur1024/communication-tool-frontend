import { DestroyRef, Injectable, OnDestroy, inject } from '@angular/core';
import { MessageHttpService } from '@services/message-http.service';
import { BehaviorSubject, Subject, catchError, concatMap, filter, switchMap, tap } from 'rxjs';
import { Message } from '@models/message.model';
import { HttpErrorHandlerService } from '@services/http-error-handler.service';
import { MessagesResponse } from '@interfaces/messages-response.interface';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { errorResponse } from '@utils/error-response.util';
import { hasResponse } from '@utils/has-response.util';

export interface MessagePayload {
  message: string;
  channelId: number;
}

@Injectable()
export class MessageLogService implements OnDestroy {

  private readonly destroy = inject(DestroyRef);

  private readonly httpService = inject(MessageHttpService);
  private readonly httpErrorHandler = inject(HttpErrorHandlerService);

  readonly messages$ = new BehaviorSubject<Message[]>([]);
  readonly isLoading$ = new BehaviorSubject<boolean>(false);

  readonly fetchMessages$ = new Subject<number | undefined>();
  readonly sendMessage$ = new Subject<MessagePayload>();

  constructor() {
    this.handleFetchMessages();
    this.handleSubmitMessage();
  }

  ngOnDestroy(): void {
    this.fetchMessages$.complete();
    this.sendMessage$.complete();
  }

  private handleFetchMessages(): void {
    this.fetchMessages$.pipe(
      filter((channelId) => !!channelId),
      tap(() => this.isLoading$.next(true)),
      switchMap((channelId) => this.httpService.fetchMessages(channelId!).pipe(
        takeUntilDestroyed(this.destroy),
        catchError((error) => {
          this.httpErrorHandler.errorMessage(error?.error?.error);
          return errorResponse<MessagesResponse>();
        })
      )),
    ).subscribe({
      next: (response) => {
        if (hasResponse(response))
          this.messages$.next(response.messages.map(m => new Message(m)));
        this.isLoading$.next(false);
      },
      error: () => this.isLoading$.next(false),
    });
  }

  private handleSubmitMessage(): void {
    this.sendMessage$.pipe(
      filter((payload) => !!payload.channelId),
      concatMap((payload) => this.httpService.sendMessage(payload).pipe(
        takeUntilDestroyed(this.destroy),
        catchError((error) => {
          this.httpErrorHandler.errorMessage(error?.error?.error);
          return errorResponse<MessagesResponse>();
        })
      ))
    ).subscribe({
      next: (response) => {
        if (hasResponse(response))
          this.messages$.next([...this.messages$.getValue(), ...(response.messages.map(m => new Message(m)))]);
      }
    });
  }

  handleNewMessage(message: Message): void {
    this.messages$.next([...this.messages$.getValue(), message]);
  }

}
