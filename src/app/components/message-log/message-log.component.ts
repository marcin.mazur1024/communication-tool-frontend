import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { MessageLogService } from './message-log.service';
import { MessagesComponent } from '@components/messages/messages.component';
import { MessageCreatorComponent } from '@components/message-creator/message-creator.component';
import { Subscription, combineLatest, map, of, startWith } from 'rxjs';
import { ChannelResolverService } from '@services/channel-resolver.service';
import { ChannelsComponent } from '@components/channels/channels.component';
import { SpinnerComponent } from '@components/spinner/spinner.component';
import { WebsocketService } from '@services/websocket.service';
import { WebsocketEventsService } from '@services/websocket-events.service';
import { Message } from '@models/message.model';

@Component({
  selector: 'ct-message-log',
  standalone: true,
  imports: [
    CommonModule,
    MessagesComponent,
    MessageCreatorComponent,
    ChannelsComponent,
    SpinnerComponent,
  ],
  styleUrl: './message-log.component.scss',
  templateUrl: './message-log.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [MessageLogService, ChannelResolverService]
})
export class MessageLogComponent implements OnInit, OnDestroy {

  private readonly messageLogService = inject(MessageLogService);
  private readonly websocetService = inject(WebsocketService);
  private readonly eventsService = inject(WebsocketEventsService);
  private readonly channelResolverService = inject(ChannelResolverService);
  private readonly subscription: Subscription = new Subscription();

  vm$ = combineLatest([
    this.messageLogService.messages$,
    this.messageLogService.isLoading$,
    this.channelResolverService.channelId$.pipe(startWith(undefined)),
    of(this.channelResolverService.canSwitchChannels)
  ]).pipe(map(([messages, isLoading, channelId, canSwitchChannels]) => ({ messages, isLoading, channelId, canSwitchChannels })));

  ngOnInit(): void {
    this.handleChannelSelection();
    this.handleNewMessageSubscription();
    this.channelResolverService.initialize();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  onSubmitMessage(message: string): void {
    if (!this.channelResolverService.channelId) return;
    this.messageLogService.sendMessage$.next({ message, channelId: this.channelResolverService.channelId });
  }

  private handleChannelSelection(): void {
    const sub = this.channelResolverService.channelId$.subscribe((channelId) => {
      this.websocetService.leavePrivateChannels();
      if (channelId) this.websocetService.joinPrivateChannel(channelId);
      this.messageLogService.fetchMessages$.next(channelId);
    });
    this.subscription.add(sub);
  }

  private handleNewMessageSubscription(): void {
    const sub = this.eventsService.newMessage$.asObservable().subscribe({
      next: (message: Message) => this.handleNewMessage(message),
    });
    this.subscription.add(sub);
  }

  private handleNewMessage(message: Message): void {
    if (message.channelId !== this.channelResolverService.channelId) return;
    if (this.channelResolverService.userContext?.id === message.userId) return;
    this.messageLogService.handleNewMessage(message);
  }

}
