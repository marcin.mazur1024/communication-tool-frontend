import { TestBed } from '@angular/core/testing';
import { AuthFormService } from './auth-form.service';
import { AuthFormType } from '@enums/auth-form-type.enum';
import { AuthHttpService } from '@services/auth-http.service';
import { AuthService } from '@services/auth.service';
import { HttpErrorHandlerService } from '@services/http-error-handler.service';
import { of, throwError } from 'rxjs';
import { AuthResponse } from '@interfaces/auth-response.interface';
import { UserRole } from '@enums/user-role.enum';
import { HttpError } from '@enums/http-error.enum';

describe('AuthFormService', () => {
  let service: AuthFormService;
  let mockAuthHttpService: jasmine.SpyObj<AuthHttpService>;
  let mockAuthService: jasmine.SpyObj<AuthService>;
  let mockHttpErrorHandlerService: jasmine.SpyObj<HttpErrorHandlerService>;

  beforeEach(() => {
    mockAuthHttpService = jasmine.createSpyObj('AuthHttpService', ['createNewAccount', 'login']);
    mockAuthService = jasmine.createSpyObj('AuthService', ['login']);
    mockHttpErrorHandlerService = jasmine.createSpyObj('HttpErrorHandlerService', ['errorMessage']);

    TestBed.configureTestingModule({
      providers: [
        AuthFormService,
        { provide: AuthHttpService, useValue: mockAuthHttpService },
        { provide: AuthService, useValue: mockAuthService },
        { provide: HttpErrorHandlerService, useValue: mockHttpErrorHandlerService }
      ]
    });
    service = TestBed.inject(AuthFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize with sign-in form', () => {
    expect(service.form.get('username')).toBeTruthy();
    expect(service.form.get('password')).toBeTruthy();
  });

  describe('submit', () => {
    it('should handle sign-up', () => {
      service.authFormType = AuthFormType.SING_UP;
      service.form.patchValue({
        username: 'pass',
        password: 'user'
      });
      const mockResponse: AuthResponse = { token: 'authToken', user: { id: 100, role: UserRole.FIELD_DEVICE, username: 'user' } };
      mockAuthHttpService.createNewAccount.and.returnValue(of(mockResponse));

      service.submit();

      expect(mockAuthHttpService.createNewAccount).toHaveBeenCalledWith(service.form.value);
      expect(mockAuthService.login).toHaveBeenCalledWith(mockResponse);
    });

    it('should handle sign-in', () => {
      service.authFormType = AuthFormType.SING_IN;
      service.form.patchValue({
        username: 'pass',
        password: 'user'
      });
      const mockResponse: AuthResponse = { token: 'authToken', user: { id: 100, role: UserRole.FIELD_DEVICE, username: 'user' } };
      mockAuthHttpService.login.and.returnValue(of(mockResponse));
      service.submit();

      expect(mockAuthHttpService.login).toHaveBeenCalledWith(service.form.value);
      expect(mockAuthService.login).toHaveBeenCalledWith(mockResponse);
    });

    it('should handle HTTP error for sign-up', () => {
      service.authFormType = AuthFormType.SING_UP;
      const errorMessage = HttpError.INVALID_INPUT_DATA;

      service.form.patchValue({
        username: 'pass',
        password: 'user'
      });

      mockAuthHttpService.createNewAccount.and.returnValue(throwError({ error: { error: errorMessage } }));
      service.submit();

      expect(mockHttpErrorHandlerService.errorMessage).toHaveBeenCalledWith(errorMessage);
    });

    it('should handle HTTP error for sign-in', () => {
      service.authFormType = AuthFormType.SING_IN;
      const errorMessage = HttpError.INVALID_CREDENTIALS;

      service.form.patchValue({
        username: 'pass',
        password: 'user'
      });

      mockAuthHttpService.login.and.returnValue(throwError({ error: { error: errorMessage } }));
      service.submit();
      expect(mockHttpErrorHandlerService.errorMessage).toHaveBeenCalledWith(errorMessage);
    });

    it('should not submit if the form is invalid', () => {
      service.authFormType = AuthFormType.SING_UP;
      const mockResponse: AuthResponse = { token: 'authToken', user: { id: 100, role: UserRole.FIELD_DEVICE, username: 'user' } };
      mockAuthHttpService.createNewAccount.and.returnValue(of(mockResponse));

      expect(service.form.invalid).toBeTrue();

      service.submit();

      expect(mockAuthHttpService.createNewAccount).not.toHaveBeenCalledWith(service.form.value);
      expect(mockAuthService.login).not.toHaveBeenCalledWith(mockResponse);
    });

  });

});
