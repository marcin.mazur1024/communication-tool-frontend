import { DestroyRef, Injectable, OnDestroy, inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthFormField } from '@enums/auth-form-field.enum';
import { AuthFormType } from '@enums/auth-form-type.enum';
import { AuthPayload } from '@interfaces/auth-payload.interface';
import { AuthResponse } from '@interfaces/auth-response.interface';
import { AuthHttpService } from '@services/auth-http.service';
import { AuthService } from '@services/auth.service';
import { HttpErrorHandlerService } from '@services/http-error-handler.service';
import { updateForm } from '@utils/update-form.util';
import { BehaviorSubject, Observable, Subject, catchError, filter, switchMap, takeUntil } from 'rxjs';
import { Destroyable } from '@utils/destroyable.util';
import { errorResponse } from '@utils/error-response.util';
import { hasResponse } from '@utils/has-response.util';

@Injectable()
export class AuthFormService extends Destroyable implements OnDestroy{

  private readonly authHttpService = inject(AuthHttpService);
  private readonly authService = inject(AuthService);
  private readonly httpErrorHandler = inject(HttpErrorHandlerService);

  private readonly _isLoading$ = new BehaviorSubject<boolean>(false);
  readonly isLoading$ = this._isLoading$.asObservable();

  private readonly request$ = new Subject<AuthPayload>();

  readonly form: FormGroup = this.initializeForm();

  private type: AuthFormType = AuthFormType.SING_IN;

  set authFormType(value: AuthFormType) {
    this.type = value;
  }

  constructor() {
    super();
    this.handleSignUp();
    this.handleSignIn();
  }

  override ngOnDestroy(): void {
      super.ngOnDestroy();
      this.request$.complete();
  }

  submit(): void {
    if (this.form.invalid) {
      updateForm(this.form)
      return;
    }
    this._isLoading$.next(true);
    this.request$.next(this.form.value);
  }

  private handleSignUp(): void {
    const req = this.request$.pipe(
      filter(() => this.type === AuthFormType.SING_UP),
      switchMap((payload) => this.authHttpService.createNewAccount(payload).pipe(
        catchError((error) => {
          this.httpErrorHandler.errorMessage(error?.error?.error);
          return errorResponse<AuthResponse>();
        })
      )),
    );
    this.handleRequest(req);
  }

  private handleSignIn(): void {
    const req = this.request$.pipe(
      filter(() => this.type === AuthFormType.SING_IN),
      switchMap((payload) => this.authHttpService.login(payload).pipe(
        catchError((error) => {
          this.httpErrorHandler.errorMessage(error?.error?.error);
          return errorResponse<AuthResponse>();
        }),
      )),
    );
    this.handleRequest(req);
  }

  private handleRequest(req: Observable<AuthResponse>): void {
    req.pipe(takeUntil(this.destroy$))
    .subscribe({
      next: (response) => {
        if (hasResponse(response)) this.authService.login(response);
        this._isLoading$.next(false);
      },
      error: () => this._isLoading$.next(false),
    });
  }

  private initializeForm(): FormGroup {
    return new FormGroup({
      [AuthFormField.USERNAME]: new FormControl(null, [Validators.required]),
      [AuthFormField.PASSWORD]: new FormControl(null, [Validators.required]),
    });
  }
}
