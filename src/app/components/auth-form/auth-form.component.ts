import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit, inject } from '@angular/core';
import { AuthFormService } from './auth-form.service';
import { combineLatest, map, startWith } from 'rxjs';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SubmitButtonComponent } from '@components/submit-button/submit-button.component';
import { PasswordModule } from 'primeng/password';
import { InputTextModule } from 'primeng/inputtext';
import { FormErrorComponent } from '@components/form-error/form-error.component';
import { updateForm } from '@utils/update-form.util';
import { AuthFormType } from '@enums/auth-form-type.enum';
import { AuthFormField } from '@enums/auth-form-field.enum';

@Component({
  selector: 'ct-auth-form',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    SubmitButtonComponent,
    PasswordModule,
    InputTextModule,
    FormErrorComponent,
  ],
  styleUrl: './auth-form.component.scss',
  templateUrl: './auth-form.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [AuthFormService],
})
export class AuthFormComponent implements OnInit {

  @Input({ required: true }) formType!: AuthFormType

  private readonly formService = inject(AuthFormService);
  private readonly cd = inject(ChangeDetectorRef);
  readonly AuthFormField = AuthFormField;

  get form(): FormGroup {
    return this.formService.form;
  }

  get password(): FormControl {
    return this.form.get(AuthFormField.PASSWORD)! as FormControl;
  }

  get username(): FormControl {
    return this.form.get(AuthFormField.USERNAME)! as FormControl;
  }

  get hasUsernameError(): boolean {
    return this.username.touched && !!this.username.errors;
  }

  get hasPasswordError(): boolean {
    return this.username.touched && !!this.username.errors;
  }

  get submitButtonLabel(): string {
    return AuthFormType.SING_UP === this.formType ? 'AuthForm.CreateAccount' : 'AuthForm.SignIn'
  }

  vm$ = combineLatest([this.formService.isLoading$.pipe(startWith(false))]).pipe(map(([isLoading]) => ({ isLoading })));

  ngOnInit(): void {
    this.formService.authFormType = this.formType;
  }

  onSubmit = (): void => {
    if (this.form.invalid) {
      updateForm(this.form);
      this.cd.markForCheck();
      return;
    }
    this.formService.submit();
  };
}
