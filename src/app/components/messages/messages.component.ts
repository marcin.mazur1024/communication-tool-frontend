import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ElementRef, Input, OnChanges, SimpleChanges, inject } from '@angular/core';
import { MessageComponent } from '@components/message/message.component';
import { Message } from '@models/message.model';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'ct-messages',
  standalone: true,
  imports: [
    CommonModule,
    MessageComponent,
    TranslateModule,
  ],
  styleUrl: './messages.component.scss',
  templateUrl: './messages.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessagesComponent implements OnChanges {

  @Input() messages: Message[] = [];
  @Input() channelId: number | undefined = undefined;

  private readonly element = inject(ElementRef);

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['messages']?.currentValue?.length > 0) this.scrollBottom();
  }

  private scrollBottom(): void {
    const element = this.element?.nativeElement;
    if (!element) return;
    setTimeout(() => element.scroll(0, element.scrollHeight));
  }

}
