import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import { RouterLink } from '@angular/router';
import { Store } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { ROUTES } from '@routes/routes.config';
import { LocalStorageService } from '@services/local-storage.service';
import { ButtonModule } from 'primeng/button';
import { combineLatest, map } from 'rxjs';
import { selectUserContext } from 'src/app/states/user/user.selector';

@Component({
  selector: 'ct-top-bar',
  standalone: true,
  imports: [
    CommonModule,
    ButtonModule,
    TranslateModule,
    RouterLink,
  ],
  template: `
  <div class="top-bar__container top-bar--fixed">
    <ng-container *ngIf="vm$ | async as vm">
      <span *ngIf="vm.userContext" class="top-bar__welcome">{{vm.userContext.username}}</span>
    </ng-container>
    <a [routerLink]="[logoutUrl]">
      <p-button label="{{ 'TopBar.Logout' | translate }}" [outlined]="true" severity="info" size="small" ></p-button>
    </a>
  </div>
  `,
  styleUrl: './top-bar.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TopBarComponent {
  readonly localStorageService = inject(LocalStorageService);
  private readonly store = inject(Store);

  vm$ = combineLatest([
    this.store.select(selectUserContext)
  ]).pipe(map(([userContext]) => ({ userContext })));

  readonly logoutUrl: string = `/${ROUTES.LOGOUT}`;

}
