import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';

@Component({
  selector: 'ct-message-creator',
  standalone: true,
  imports: [
    CommonModule,
    InputTextareaModule,
    FormsModule,
    ButtonModule,
    TranslateModule
  ],
  template: `
  <div class="message-creator__container">
      <textarea pInputTextarea [(ngModel)]="message" [placeholder]="'Messages.TypeMessage' | translate" [disabled]="!channelId"></textarea>
      <p-button
      icon="pi pi-send"
      [disabled]="!message.length"
      (onClick)="submitMessage.next(message); message = '';"
      ></p-button>
  </div>
  `,
  styleUrl: './message-creator.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageCreatorComponent {
  @Output() submitMessage = new EventEmitter<string>();
  @Input() channelId: number | undefined;
  message: string = '';
}
