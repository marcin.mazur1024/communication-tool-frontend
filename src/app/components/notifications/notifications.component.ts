import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, inject } from '@angular/core';
import { NotificationPayload } from '@interfaces/notification-payload.interface';
import { TranslateService } from '@ngx-translate/core';
import { NotificationService } from '@services/notification.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ct-notifications',
  standalone: true,
  imports: [
    CommonModule,
    ToastModule,
  ],
  template: `<p-toast></p-toast>`,
  styleUrl: './notifications.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [MessageService]
})
export class NotificationsComponent implements OnInit, OnDestroy {

  private readonly m = inject(MessageService);
  private readonly t = inject(TranslateService);
  private readonly n = inject(NotificationService);
  readonly subscription = new Subscription();

  ngOnInit(): void {
    const sub = this.n.notify$.subscribe({
      next: (payload: NotificationPayload) => this.add(payload)
    })
    this.subscription.add(sub);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private add(payload: NotificationPayload): void {
    this.m.add({
      severity: payload.severity,
      summary: this.resolveText(payload.title, !payload.skipTranslate),
      detail: this.resolveText(payload.message, !payload.skipTranslate),
    });
  };

  private resolveText(m: string | undefined, useTranslate: boolean): string | undefined {
    return useTranslate && m ? this.t.instant(m) : m;
  };

}
