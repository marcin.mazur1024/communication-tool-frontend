import { ComponentFixture, TestBed } from "@angular/core/testing";
import { NotificationsComponent } from "./notifications.component";
import { TranslateModule } from "@ngx-translate/core";
import { NotificationService } from "@services/notification.service";
import { NotificationPayload } from "@interfaces/notification-payload.interface";
import { Subject } from "rxjs";
import { MessageService } from "primeng/api";

class NotificationServiceMock {
  private readonly _notify$ = new Subject<NotificationPayload>();
  readonly notify$ = this._notify$.asObservable();
}

describe('NotificationsComponent', () => {

  let component: NotificationsComponent;
  let fixture: ComponentFixture<NotificationsComponent>;
  let notificationServiceMock: jasmine.SpyObj<NotificationService>;

  beforeEach(async () => {

    await TestBed.configureTestingModule({
      imports: [
        NotificationsComponent,
        TranslateModule.forRoot({}),
      ],
      providers: [
        {
          provide: NotificationService,
          useClass: NotificationServiceMock
        },
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(NotificationsComponent);
    component = fixture.componentInstance;
    notificationServiceMock = TestBed.inject(NotificationService) as jasmine.SpyObj<NotificationService>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add a message on Observeable emit', async () => {
    const spy = spyOn<NotificationsComponent, any>(component, 'add')
    const notiPayload: NotificationPayload = {
      message: 'testMessage',
      severity: 'success',
      title: 'testTitle'
    }
    notificationServiceMock['_notify$'].next(notiPayload);
    expect(spy).toHaveBeenCalledWith(notiPayload);
  });

  it('should unsubscribe on ngOnDestroy', () => {
    component.ngOnDestroy();
    expect(component.subscription.closed).toBeTrue();
  });

})
