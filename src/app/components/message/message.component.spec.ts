import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Message } from '@models/message.model';
import { MessageComponent } from './message.component';
import { provideStore } from '@ngrx/store';

describe('MessageComponent', () => {
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;

  const message: Message = {
    id: 100,
    content: 'Test message',
    channelId: 100,
    createdAt: new Date(),
    userId: 100,
    username: 'user'
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MessageComponent
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageComponent);
    component = fixture.componentInstance;
    component.message = message;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render username', () => {
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.message__username').textContent).toContain(message.username);
  });
});
