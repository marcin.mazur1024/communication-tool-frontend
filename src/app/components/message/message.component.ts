import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Message } from '@models/message.model';

@Component({
  selector: 'ct-message',
  standalone: true,
  imports: [
    CommonModule,
  ],
  styleUrl: './message.component.scss',
  templateUrl: './message.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageComponent {
  @Input({ required: true }) message!: Message;
}
