import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RouterLink } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

@Component({
  selector: 'ct-auth-form-wrapper',
  standalone: true,
  imports: [CommonModule, TranslateModule, RouterLink],
  styleUrl: './auth-form-wrapper.component.scss',
  templateUrl: './auth-form-wrapper.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthFormWrapperComponent {
  @Input({ required: true }) headerlabel!: string;
  @Input({ required: true }) footerLabel!: string;
  @Input({ required: true }) footerUrl!: string;
}
