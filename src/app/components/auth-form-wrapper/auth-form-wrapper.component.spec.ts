import { ComponentFixture, TestBed } from "@angular/core/testing";
import { AuthFormWrapperComponent } from "./auth-form-wrapper.component";
import { TranslateModule } from "@ngx-translate/core";
import { RouterModule } from "@angular/router";

describe('AuthFormWrapperComponent', () => {

  let component: AuthFormWrapperComponent;
  let fixture: ComponentFixture<AuthFormWrapperComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [AuthFormWrapperComponent, TranslateModule.forRoot({}), RouterModule.forRoot([]),],
      providers: []
    }).compileComponents();

    fixture = TestBed.createComponent(AuthFormWrapperComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display a url when the footerUrl property is provided', async () => {

    const label = 'testLabel';
    const url = '/user/test';

    component.footerUrl = url;
    component.footerLabel = label;

    fixture.detectChanges();
    await fixture.whenStable();

    const anchor: HTMLElement = fixture.debugElement.nativeElement.querySelector('a');
    expect(anchor).toBeTruthy();

    expect(anchor.innerText).toBe(label);
    expect(anchor.getAttribute('href')).toBe(url);

  });

})
