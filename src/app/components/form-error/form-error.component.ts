import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';
import { FormError } from '@enums/form-error.enum';
import { TranslateModule } from '@ngx-translate/core';
import { BehaviorSubject, combineLatest, map, startWith } from 'rxjs';

@Component({
  selector: 'ct-form-error',
  standalone: true,
  imports: [
    CommonModule,
    TranslateModule,
  ],
  template: `
    <ng-container *ngIf="vm$ | async as vm">
      <span *ngIf="vm.errorMessage" class="form-error" [innerText]="vm.errorMessage | translate"></span>
    </ng-container>
  `,
  styleUrl: './form-error.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormErrorComponent {

  private readonly _error$ = new BehaviorSubject<string | undefined>(undefined);

  @Input() set errors(errors: ValidationErrors | undefined | null) {
    this.prepareEmptyMessage(errors);
    this.prepareErrorMessage(errors);
  };

  vm$ = combineLatest([this._error$.asObservable().pipe(startWith(undefined))]).pipe(map(([errorMessage]) => ({ errorMessage })));

  private prepareErrorMessage(errors: ValidationErrors | undefined | null): void {
    if (!errors) return;
    for (const [key, value] of Object.entries(errors)) {
      this._error$.next(this.resolveErrorMessage(key as FormError));
      break;
    }
  }

  private prepareEmptyMessage(errors: ValidationErrors | undefined | null): void {
    if (errors) return;
    this._error$.next(undefined);
  }

  private resolveErrorMessage(error: FormError): string {
    switch (error) {
      case FormError.REQUIRED:
        return 'Validation.FieldRequired';
      default:
        return 'Validation.General';
    }
  }
}
