import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormErrorComponent } from "./form-error.component";
import { ValidationErrors } from "@angular/forms";
import { TranslateModule } from "@ngx-translate/core";

describe('FormErrorComponent', () => {

  let component: FormErrorComponent;
  let fixture: ComponentFixture<FormErrorComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [FormErrorComponent, TranslateModule.forRoot({})]
    }).compileComponents();
    fixture = TestBed.createComponent(FormErrorComponent);
    component = fixture.componentInstance
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a message when the input value is not undefined', async () => {
    const errorMsg: string = 'Validation.FieldRequired';
    const spy = spyOn<FormErrorComponent, any>(component, 'resolveErrorMessage').and.returnValue(errorMsg);
    component.errors = { required: true } as ValidationErrors;

    fixture.detectChanges();
    await fixture.whenStable();
    const errorSpan = fixture.debugElement.nativeElement.querySelector('.form-error');

    expect(spy).toHaveBeenCalled();
    expect(errorSpan).toBeTruthy();
    expect(errorSpan.innerText).toBe(errorMsg);
  });

  it('should not render a message when the input value is undefined', async () => {
    component.errors = undefined;
    fixture.detectChanges();
    await fixture.whenStable();
    const errorSpan = fixture.debugElement.nativeElement.querySelector('.form-error');
    expect(errorSpan).toBeFalsy();
  });

});
