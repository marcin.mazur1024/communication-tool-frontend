import { Environment } from '@interfaces/environment.interface';
import { ROUTES } from '@routes/routes.config';

export const environment: Environment = {
  production: true,
  apiUrl: 'http://127.0.0.1:8000/api/',
  baseAuthRoute: ROUTES.COMMUNICATOR,
  baseUnAuthRoute: ROUTES.SIGN_IN,
  wssAuthUrl: 'http://127.0.0.1:8000/api/broadcasting/auth',
  wssBroadcaster: 'reverb',
  wssHost: '127.0.0.1',
  wssPort: 8080,
  wssKey: 'wyvicmomdhkxb2mtwiy8',
  wssForceTls: false,
  wssDisableStats: true,
};
